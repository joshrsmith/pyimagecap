#!/usr/bin/env python

'''
Created on Dec 14, 2013

@author: josh
'''

import urllib2
import os
import datetime
import time
import signal
import multiprocessing
import logging
import argparse
import errno
import ssl

import ephem

from SharedUtil import DateTime2Path, DateTime2ImageName
import _version


class EmptyImageException(Exception):
    pass


stop_event = multiprocessing.Event()


def stop(signum, frame):
    stop_event.set()
signal.signal(signal.SIGTERM, stop)


class ImageCapturer(object):
    def __init__(self, url, destDir, interval=60,
                 onlySunUp=True, latitude=0.0, longitude=0.0):
        self.url = url
        self.destDir = destDir
        self.interval = interval
        self.onlySunUp = onlySunUp
        self.latitude = latitude
        self.longitude = longitude
        # purposely leave out certificate authentication
        # this is because cameras may use self=signed certificates will throw
        # warnings.
        try:
            self.sslcontext = ssl._create_unverified_context()
        except:
            self.sslcontext = None

    def fetch_img(self):
        if self.sslcontext is not None:
            urldata = urllib2.urlopen(self.url, context=self.sslcontext)
        else:
            urldata = urllib2.urlopen(self.url)
        image = urldata.read()
        urldata.close()
        if len(image) == 0:
            raise EmptyImageException("No data was returned in fetched image")
        return image

    def capture(self):
        total_errors = 0
        while not stop_event.is_set():
            if self.onlySunUp and not SunUp(latitude=self.latitude,
                                            longitude=self.longitude,
                                            when=ephem.now()):
                next_capture_time = NextSunrise(latitude=self.latitude,
                                                longitude=self.longitude,
                                                after=ephem.now())

                sleep_delta = next_capture_time - datetime.datetime.now()
                logging.info("Sun not up, waiting for next sunrise, wait "
                             "time = %.1f hours"
                             % (sleep_delta.total_seconds() / 3600,))
                logging.info("Next sunrise = " + next_capture_time.isoformat())
                time.sleep(sleep_delta.total_seconds())
                time.sleep(60)
                logging.info("Time to check if sun is up")
            else:
                cap_time = datetime.datetime.now()
                next_cap = cap_time + datetime.timedelta(seconds=self.interval)
                try:
                    image = self.fetch_img()
                    dest_path = os.path.join(self.destDir,
                                             DateTime2Path(cap_time))
                    if not os.path.exists(dest_path):
                        os.makedirs(dest_path)
                    image_filename = DateTime2ImageName(cap_time) + ".jpg"
                    with open(os.path.join(dest_path, image_filename), 'wb') as image_file:
                        image_file.write(image)
                    total_errors = 0

                    sleep_delta = next_cap - datetime.datetime.now()
                    if sleep_delta.total_seconds() > 0:
                        time.sleep(sleep_delta.total_seconds())
                except (urllib2.HTTPError, urllib2.URLError) as e:
                    logging.warning(repr(e))
                    time.sleep(1)
                    total_errors += 1
                    if total_errors > 10:
                        logging.warning(">10 errors in a row")
                        time.sleep(30)
                except EmptyImageException, e:
                    logging.warning("EmptyImageException: " + repr(e))
                    time.sleep(10)
                except IOError, e:
                    if e.errno == 54:
                        logging.warning(errno.errorcode[e.errno] + repr(e))
                    else:
                        raise e


def SunUp(latitude, longitude, when=ephem.now()):
    o = ephem.Observer()
    o.lat = "%f" % latitude
    o.long = "%f" % longitude
    o.date = when
    s = ephem.Sun()
    s.compute()
    if ephem.localtime(o.next_rising(s)) > ephem.localtime(o.next_setting(s)):
        return True
    else:
        return False


def NextSunrise(latitude, longitude, after=ephem.now()):
    o = ephem.Observer()
    o.lat = "%f" % latitude
    o.long = "%f" % longitude
    o.date = after
    s = ephem.Sun()
    s.compute()
    return ephem.localtime(o.next_rising(s))


def main():
    parser = argparse.ArgumentParser(description='Capture images from URL')
    parser.add_argument("url",
                        help="URL to capture images from")
    parser.add_argument("dest",
                        help="Directory to store the captured images")
    parser.add_argument("--interval", default=5,
                        help="Seconds between capturing images",)
    parser.add_argument("--sunup", action='store_true',
                        help="When present, only capture when sun is up")
    parser.add_argument("--lat", type=float, default=None,
                        help="Latitude of the camera (+N, degrees)")
    parser.add_argument("--long", type=float, default=None,
                        help="Longitude of the camera (+E, degrees)")
    parser.add_argument('--version', action='version',
                        version='%(prog)s ' + _version.__version__)

    args = parser.parse_args()
    sun_up_only = args.sunup

    if sun_up_only and (args.lat is None or args.long is None):
        parser.error("Must specify latitude and longitude when only recording "
                     "during sunup.")

    interval_seconds = int(args.interval)
    imageUrl = args.url
    basedir = args.dest

    logging.basicConfig(filename=os.path.join(basedir,
                                              '%s.log' % (parser.prog)),
                        level=logging.DEBUG,
                        format='%(asctime)s %(message)s')
    logging.info('Starting up')
    logging.info("Capturing from: " + imageUrl)
    logging.info("Capture Destination: " + basedir)
    logging.info("Capture Interval (seconds): " + str(interval_seconds))
    logging.info("Capture Only when Sun is Up: " + str(sun_up_only))

    if args.lat is not None:
        logging.info("Camera Latitude = %f" % (args.lat,))
    if args.long is not None:
        logging.info("Camera Longitude = %f" % (args.long,))

    try:
        ic = ImageCapturer(imageUrl, basedir, interval_seconds,
                           onlySunUp=sun_up_only,
                           latitude=args.lat, longitude=args.long)
        ic.capture()
        logging.info('Shutting down')
    except Exception, e:
        logging.error("Exception: " + repr(e))
        raise


if __name__ == "__main__":
    main()
