'''
Created on Dec 14, 2013

@author: josh
'''

import datetime
import os


def DateTime2Path(dateTime=datetime.datetime.now()):
    """Get a file path for the provided time.

    Will be in the format YYYY/MM/DD/HH"""
    month = "%02d" % (dateTime.month,)
    day = "%02d" % (dateTime.day,)
    hour = "%02d" % (dateTime.hour,)
    path = os.path.join(str(dateTime.year), month, day, hour)
    return path


def DateTime2ImageName(dateTime=datetime.datetime.now()):
    """Get an filename for the provided time.

    Name will be in the format YYYYMMDDHHmmss."""
    month = "%02d" % (dateTime.month,)
    day = "%02d" % (dateTime.day,)
    hour = "%02d" % (dateTime.hour,)
    minute = "%02d" % (dateTime.minute,)
    second = "%02d" % (dateTime.second,)
    image_name = str(dateTime.year) + month + day + hour + minute + second
    return image_name
