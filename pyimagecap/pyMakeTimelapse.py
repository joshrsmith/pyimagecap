#!/usr/bin/env python

"""A script to automate the creation of timelapse videos.
Currently it assumes that the source images were collected
at a rate of 1 image every 5 seconds.
"""

import datetime
import os
import glob
import subprocess
import shutil
import logging
import argparse

import tempdir

from SharedUtil import DateTime2Path
import _version


def RelativePathsToSearch(startDateTime, endDateTime):
    """Get a list of strings representing paths to directories containing
    images that were collected between startDateTime and endDateTime"""
    paths = []
    current_time = startDateTime
    while current_time <= endDateTime:
        paths.append(DateTime2Path(current_time))
        current_time += datetime.timedelta(hours=1)
    return paths


def ImageList(baseDir, paths):
    """Generate a list of absolute paths of images by looking
    for jpg images in the list of provided paths."""
    imgfiles = []
    for path in paths:
        imgfiles.extend(sorted(glob.glob(os.path.join(baseDir,
                                                      path, '*.jpg'))))
    return imgfiles


def ImageTimeDiff(image1, image2):
    """Calculate the time delta, in seconds, between two images
    based on their filename."""
    file1 = os.path.splitext(os.path.basename(image1))
    file2 = os.path.splitext(os.path.basename(image2))
    datetimestr1 = file1[0]
    datetimestr2 = file2[0]
    datetime1 = datetime.datetime.strptime(datetimestr1, "%Y%m%d%H%M%S")
    datetime2 = datetime.datetime.strptime(datetimestr2, "%Y%m%d%H%M%S")
    time_delta = datetime2 - datetime1
    return abs(time_delta.total_seconds())


class TimelapseCreator(object):
    def __init__(self, imageDirectory, workingDirectory):
        """Setup TimelapseCreator.

        imageDirectory - Base directory where images have been stored.
            It is assumed that images are stored in the following manner:
                YYYY/MM/DD/HH/<images>
            Furthermore, it is assumed that images are named in the following
            manner:
                YYYYMMDDHHmmss.jpg
        workingDirectory - The output file will be stored here. Also,
            a temporary directory will be created here for processing.
        """

        self.imageDirectory = imageDirectory
        self.workingDirectory = workingDirectory

    def make_timelapse(self, startDateTime, duration):
        """Create a timelapse video by using images in the imageDirectory.

        startDateTime - The first image found in the imageDirectory
            after this time will be the beginning of the video.

        duration - the type of timelapse that is being created, used
            for scaling the speed of the timelapse video.
        """
        endDateTime = startDateTime  # default
        fps = 30  # default
        if duration == "day":
            decimate_interval = 1
            fps = 60
            endDateTime = startDateTime + datetime.timedelta(days=1)
            output_fname = "DAY_" + startDateTime.date().isoformat()
        elif duration == "week":
            decimate_interval = 12
            endDateTime = startDateTime + datetime.timedelta(days=7)
            output_fname = "WEEK_" + startDateTime.date().isoformat() + \
                "_" + endDateTime.date().isoformat()
        elif duration == "month":
            decimate_interval = 12 * 4
            endDateTime = startDateTime + datetime.timedelta(days=31)
            output_fname = "MONTH_" + startDateTime.date().isoformat()
        elif duration == "year":
            decimate_interval = 12 * 15
            endDateTime = startDateTime + datetime.timedelta(days=365)
            output_fname = "YEAR_" + startDateTime.date().isoformat()
        elif duration == "test":
            decimate_interval = 12
            endDateTime = startDateTime + datetime.timedelta(hours=9)
            output_fname = "TEST_" + startDateTime.date().isoformat()
        else:
            raise Exception
        output_fname += ".mp4"
        logging.info("End of time period: " + endDateTime.date().isoformat())

        paths = RelativePathsToSearch(startDateTime, endDateTime)

        # Get the full list of images contained in all directories
        # corresponding to the day/time range that we are working with
        imgfiles = ImageList(self.imageDirectory, paths)

        numImg = len(imgfiles)
        logging.info("Num input images: " + str(numImg))

        # Figure out the average time elapsed between each image
        timediff = 0
        images_to_average = min(numImg, 10)
        for i in range(images_to_average - 1):
            timediff += ImageTimeDiff(imgfiles[i], imgfiles[i + 1])
        avg_interval_seconds = round(timediff / images_to_average)
        logging.info("Average interval, seconds: " + str(avg_interval_seconds))

        try:
            tdir = tempdir.TempDir(basedir=self.workingDirectory)
            count = 0
            for i in range(len(imgfiles)):
                if i % (decimate_interval) == 0:
                    os.symlink(imgfiles[i],
                               os.path.join(tdir.name,
                                            "img%09d.jpg" % (count,)))
                    count += 1
            logging.info("Actual frames to encode: " + str(count))
            logging.info("Expected duration: %.2f" % (count / fps,))
            arglist = ["-framerate", str(fps),  # input frame rate
                       "-f", "image2",
                       "-i", os.path.join(tdir.name, 'img%09d.jpg'),
                       "-r", str(fps),  # output frame rate
                       "-qscale", str(8),
                       os.path.join(self.workingDirectory, output_fname)
                       ]
            call = ["avconv"]
            call.extend(arglist)
            logging.debug("ffmpeg call: " + " ".join(call))
            subprocess.check_output(call)
        finally:
            tdir.dissolve()


def main():
    parser = argparse.ArgumentParser(description='Generate a timelapse video')
    parser.add_argument("src",
                        help="Base directory to look for image data")
    parser.add_argument("datestr",
                        help="Date string for start of timelapse YYYYMMDD "
                        "format")
    parser.add_argument("duration",
                        help="Possible values are day week month year")
    parser.add_argument("workingDir",
                        help="Directory to store temporary files, and output "
                        "file")
    parser.add_argument('--version', action='version',
                        version='%(prog)s ' + _version.__version__)

    args = parser.parse_args()
    basedir = args.src
    startdate = datetime.datetime.strptime(args.datestr, "%Y%m%d")
    duration = args.duration
    workDir = args.workingDir

    if (duration != "day"
            and duration != "week"
            and duration != "month"
            and duration != "year"
            and duration != "test"):
        parser.error("Bad duration specified")

    logging.basicConfig(filename=os.path.join(os.getcwd(),
                                              '%s.log' % (parser.prog,)),
                        level=logging.DEBUG,
                        format='%(asctime)s %(message)s')
    logging.info('Starting up %s v%s' % (parser.prog, _version.__version__))
    logging.info("Images Directory: " + basedir)
    logging.info("Start Date: " + startdate.date().isoformat())
    logging.info("Duration: " + duration)
    logging.info("Working Directory: " + workDir)

    logging.info("Starting video creation")
    tc = TimelapseCreator(basedir, workDir)
    tc.make_timelapse(startdate, duration)
    logging.info("Completed creation")


if __name__ == "__main__":
    main()
