
from setuptools import setup

exec(open('pyimagecap/_version.py').read())

try:
    __version__
except Exception:
    __version__ = "0.0"

setup(
   name="pyimagecap",
   version=__version__,
   description="Capture still images from webcam. Make them into timelapse videos.",
   author="Josh Smith",
   author_email="joshrsmith@gmail.com",
   maintainer="Josh Smith",
   maintainer_email="joshrsmith@gmail.com",
   license="MIT",
   url="",
   packages=['pyimagecap', ],
   install_requires=[
          'pyephem',
          'tempdir',
          ],
    entry_points={
        'console_scripts': [
            'pyimgcap = pyimagecap.pyImageCap:main',
            'pytimelapse = pyimagecap.pyMakeTimelapse:main',
            ],
          }
      )
